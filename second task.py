import shelve


def added():
    db = shelve.open("dict.db", "c")
    while True:
        link = input("Enter a link: ")
        name = input("Enter a name link: ")
        db[name] = link
        question = str(input("Add another link? Yes or No: "))
        if question == "Yes":
            continue
        else:
            break
    for key, value in db.items():
        print(key, ":", value, end="\n")
    print()


def find_link():
    url = shelve.open("dict.db", "c")
    while True:
        try:
            keys = input("Enter a name link: ")
            print(url[keys])
        except KeyError:
            print("This key is missing")
        question_2 = str(input("Choose another link? Yes or No: "))
        if question_2 == "Yes":
            continue
        else:
            break
    print()


def main():
    print("""Select an action:
    1. Add link
    2. Find link
    3. Exit""")
    while True:
        sel_action = int(input("Enter a number action: "))
        if sel_action == 1:
            added()
            continue
        elif sel_action == 2:
            find_link()
            continue
        elif sel_action == 3:
            print("You have exited the program")
            break
        else:
            print("Error")


if __name__ == "__main__":
    main()










