summa = "text_1.txt"

with open(summa, "r+") as summa_numbers:
    numbers = [int(line) for line in summa_numbers.readlines() if line != '\n']
    sum_ = sum(numbers)
    print("Summa: {}".format(sum_), file=summa_numbers)
